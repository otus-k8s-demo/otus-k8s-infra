# IAC: Deploying Yandex Managed Kubernetes Cluster via Terraform

## Yandex Cloud: YMS for Kubernetes

1. connect to Ya.Cloud --> create billing account --> verify
2. Install ya.cli, terraform, tfswitch, kubectl, kubectx, kubens
3. create profile
4. create cloud --> create folder
5. create networks and subnets via tf
6. create SA via tf
7. Get OAUTH token via 'yc init' (yc cli tool)
8. Create [YMS for Kubernetes](https://cloud.yandex.ru/docs/managed-kubernetes/operations/kubernetes-cluster/)

kubernetes-cluster-create:

   ```sh
       cd yc-k8s-infra
       tfswitch
       terraform init
       terraform plan
       terraform apply
   ```

9. Adding k8s-cluster to the kubectl config:

   ```sh
       yc managed-kubernetes cluster get-credentials k8s-test-cluster --external
   ```

10. Checking k9s-cluster:

   ```sh
       k get cluster-info
   ```
