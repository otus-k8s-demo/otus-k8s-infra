provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = "ru-central1-a"
}



resource "yandex_kubernetes_cluster" "k8s-cluster" {
  name        = "k8s-cluster"
  description = "k8s microservices-demo cluster"
  network_id  = yandex_vpc_network.k8s-network-1.id

  master {
    version = "1.19"
    zonal {
      zone      = yandex_vpc_subnet.k8s-subnet-1.zone
      subnet_id = yandex_vpc_subnet.k8s-subnet-1.id
    }
    public_ip = true
  }

  service_account_id      = yandex_iam_service_account.k8s-sa.id
  node_service_account_id = yandex_iam_service_account.k8s-sa.id
  release_channel = "RAPID"
  depends_on = [yandex_resourcemanager_folder_iam_member.default]
  #kms_provider {
  #  key_id = "<идентификатор ключа шифрования>"
  #}
}

resource "yandex_kubernetes_node_group" "default-pool" {
  
  cluster_id  = yandex_kubernetes_cluster.k8s-test-cluster.id
  name        = "default-pool"
  description = "default-pool"
  version     = "1.19"
  
  instance_template {
    
    platform_id = "standard-v2"
    nat         = true
    metadata    = {
    
    # TO DO: double-check how to connect via ssh to node
    ssh-keys = "yc-user:${file(var.public_key_path)}"
  }
  

    resources {
      cores         = 2
      core_fraction = 50
      memory        = 4
    }
    boot_disk {
      
      size = 30
      
    }
    
}
  scale_policy  {
    fixed_scale {
      size = 1
    }
  }
}

resource "yandex_vpc_network" "k8s-network-1" {
  name = "k8s-network-1"
}

resource "yandex_vpc_subnet" "k8s-subnet-1" {
  network_id     = yandex_vpc_network.k8s-network-1.id
  zone           = "ru-central1-a"
  v4_cidr_blocks = ["192.168.20.0/24"]
  depends_on = [
    yandex_vpc_network.k8s-network-1,
  ]
}

resource "yandex_iam_service_account" "k8s-sa" {
  name = "k8s-sa"
}

resource "yandex_resourcemanager_folder_iam_member" "default" {
  folder_id = var.folder_id

  member = "serviceAccount:${yandex_iam_service_account.k8s-sa.id}"
  role   = "editor"
}

locals {
  kubeconfig = <<KUBECONFIG
apiVersion: v1
clusters:
- cluster:
    server: ${yandex_kubernetes_cluster.k8s-test-cluster.master[0].external_v4_endpoint}
    certificate-authority-data: ${base64encode(yandex_kubernetes_cluster.k8s-test-cluster.master[0].cluster_ca_certificate)}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: yc
  name: ycmk8s
current-context: ycmk8s
users:
- name: yc
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: yc
      args:
      - k8s
      - create-token
KUBECONFIG
}


output "kubeconfig" {
  value = local.kubeconfig
}

