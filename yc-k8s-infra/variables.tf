variable cloud_id {
  description = "Yandex Cloud"
}

variable folder_id {
  description = "default folder"
}

variable service_account_id {}

variable node_service_account_id {}

variable subnet_id {}

variable network_id {
  description = "folder network-id"
}

variable zone {
  description = "Zone"
  default     = "ru-central1-a"
}

variable public_key_path {
  default = "~/.ssh/ycvm-user.pub"
}

variable token {
  description = "token"
}