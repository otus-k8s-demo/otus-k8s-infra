# otus-k8s-project: microservices-demo

1. Deploy YMS for Kubernetes via [TF](./yc-k8s-infra/README.md)

2. Deploy ArgoCD [manifests](./argocd/README.md)

3. Deploy Sockshop via ArgoCD

4. Deploy Monitoring stack (PG) via ArgoCD

5. Deploy Logging stack (ELK) via ArgoCD

6. Optional: deploy tracing stack (jaeger) via ArgoCD

7. Optional: deploy alerting (AlertManager) via ArgoCD

Additional information:

- [Gitlab group of projects](https://gitlab.com/otus-k8s-demo)
- My fork of [microservices-demo with fixes](https://gitlab.com/otus-k8s-demo/microservices-demo)
- [infra](https://gitlab.com/otus-k8s-demo/otus-k8s-infra)
- Sockshop demo [app](https://microservices-demo.github.io/deployment/kubernetes-minikube.html)
- [microservices repos](https://github.com/orgs/microservices-demo/repositories?type=all)
- [original microservices-demo](https://github.com/microservices-demo/microservices-demo)

8. SockShop Demo [Roadmap](Roadmap.md)

Owners of projects: @mzabolotnov and @s.zolotov92
