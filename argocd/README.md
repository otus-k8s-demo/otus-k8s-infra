# GitOps approach of CD for all applications: ArgoCD

[Official docs](https://argo-cd.readthedocs.io/en/stable/getting_started/)

1. After infra deployment need to install ArgoCD into YMKS cluster.

2. Installation:

    ```sh
        kubectl create namespace argocd
        kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```

3. Change the argocd-server service type to LoadBalancer: (if it is needed, for minikube --> change to NodePort type)

    ```sh
        kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
    ```

4. Get login password via kubectl and login to argocd (ui/cli):

    ```sh
        kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
        argocd login <ARGOCD_SERVER>
        argocd account update-password  # <-- optional
    ```

5. Create deploy token in gitlab-project: [microservices-demo](https://gitlab.com/otus-k8s-demo/microservices-demo)

    ```sh
        Token Creds: microtoken = RBj-_nQaTF3yj_e7YRK_
    ```

6. Add microservices-demo gitlab-repo and secrets into ArgoCD:

    ```sh
        kubectl apply -f sockshop-add-repo-secret.yml
    ```

7. Add projects and apps/paths via argocd-cli/UI/manifests or use default project:

Manually via UI:

Steps:

- create [sock-shop project/application](https://gitlab.com/otus-k8s-demo/microservices-demo/-/tree/master/deploy/kubernetes/manifests)
- create [monitoring project/application](https://gitlab.com/otus-k8s-demo/microservices-demo/-/tree/master/deploy/kubernetes/manifests-monitoring)
- create [logging project/application](https://gitlab.com/otus-k8s-demo/microservices-demo/-/tree/master/deploy/kubernetes/manifests-logging)

optional:

- create [tracing project/application](https://gitlab.com/otus-k8s-demo/microservices-demo/-/tree/master/deploy/kubernetes/manifests-jaeger)
- create [alerting project/application](https://gitlab.com/otus-k8s-demo/microservices-demo/-/tree/master/deploy/kubernetes/manifests-alerting)

or Apply manifests:

- sockshop app

    ```sh
        cd argocd/
        kubectl apply -f sockshop-app/sockshop-project.yml
        kubectl apply -f sockshop-app/sockshop-app.yml
    ```

- monitoring (optional: alerting, PGA)

    ```sh
        kubectl apply -f monitoring/monitoring-project.yml
        kubectl apply -f monitoring/monitoring-stack.yml
        kubectl apply -f monitoring/alerting.yml
    ```

- loggigng (ELK)

    ```sh
        kubectl apply -f logging/logging-project.yml
        kubectl apply -f logging/logging-stack.yml
    ```

- optional: tracing (jaeger, test usage only!)

    ```sh
        kubectl apply -f tracing/tracing-project.yml
        kubectl apply -f tracing/jaeger.yml
    ```

8. Click "Sync" button for each application, that we have

## P.S

1. Alerting doesn't work due to otus slack-channel access/rights issues
2. argocd-ingress.yml is not ready yet. [ingress docs](https://argo-cd.readthedocs.io/en/stable/operator-manual/ingress/)
3. Also all apps above will be work if we deploy helm-chart
