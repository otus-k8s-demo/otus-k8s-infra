# This is a roadmap of improving K8S platform for SockShop

1. Some tuning of IAC: YMKS cluster (using terragrunt module for example...)
2. CI: reconfigure all CI pipelines of [apps](https://github.com/orgs/microservices-demo/repositories?type=all) from travis-ci to gitlab-ci
3. CI: Push new images and helm-charts to Storage System Tool, (GitLab/Harbor/ECR/GCR..)
4. CD: ArgoCD / GitOps
   - tune [Declarative Setup](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/)
   - migrating to helm-charts instead of manifests/path-based integration
   - [Manage Argo CD Using Argo CD](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#manage-argo-cd-using-argo-cd)
   - add nginx-ingress for ArgoCD
   - enable Auto-Sync mode for SockShop App
5. Fix AlertManager, integration with PagerDuty
6. Need more resources for ELK stack
7. Need more resources for YKMS (nodes, ha)